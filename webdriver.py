from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as GeckoOptions

from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


DEFAULT_USERAGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6)\
AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8'


class SeleniumBrowser:
    ''' Controls the WebDriver and allows to drive the browser.
    '''

    def __init__(self, capabilities=None,
                 options=None, timeout=30):
        '''
        :param log_path: Path to the driver log information.
        :type log_path: str

        :param capabilities: Dictionary object with non-browser specific
        capabilities only, such as "proxy" or "PageLoadStrategy".
        :type capabilities: dict

        :param options: Dictionary object with specific driver Options
        class, sych as user agent, window size.
        :type options: dict

        :param timeout: Time needs browser for page load and implicity
        timeout.
        :type timeout: int
        '''
        if not capabilities:
            capabilities = dict()
        if not options:
            options = dict()

        self.timeout = timeout
        self.screen = options.get('screen_size', (1080, 1920))
        self.user_agent = options.get('user_agent', DEFAULT_USERAGENT)
        self.proxy = capabilities.get('proxy')
        self.log_path = capabilities.get('log_path', '/dev/null')
        self.insecure_certs = capabilities.get('acceptInsecureCerts', True)
        self.load_strategy = capabilities.get('acceptInsecureCerts', 'none')

    def init_chrome(self, path='chromedriver',
                    binary='/usr/lib/chromium/chrome'):
        ''' Create new instance of chrome web browser.

        :param path: Path to the executable. If the default is used
        it assumes the executable is in the $PATH.
        :type path: str
        '''
        # init config objects
        options = ChromeOptions()
        capa = DesiredCapabilities.CHROME.copy()

        # configure options
        options.set_headless()
        # chrome specific preference
        options.add_argument('--no-sandbox')
        options.binary_location = binary
        options.add_argument(f'user-agent={self.user_agent}')
        if self.proxy:
            options.add_argument(f'--proxy-server={self.proxy}')

        # configure desire capabilities
        capa['pageLoadStrategy'] = self.load_strategy
        capa['acceptInsecureCerts'] = self.insecure_certs

        driver = webdriver.Chrome(desired_capabilities=capa,
                                  executable_path=path,
                                  options=options,
                                  service_log_path=self.log_path)

        driver.implicitly_wait(self.timeout)
        driver.set_page_load_timeout(self.timeout)
        driver.set_window_size(*self.screen)
        return driver

    def init_firefox(self, path='geckodriver',
                     binary='/usr/lib/firefox/firefox'):
        ''' Create new instance of firefox web browser.

        :param path: Path to the executable. If the default is used
        it assumes the executable is in the $PATH.
        :type path: str
        '''
        # init config objects
        profile = webdriver.FirefoxProfile()
        capa = DesiredCapabilities.FIREFOX.copy()
        binary = FirefoxBinary(binary)
        options = GeckoOptions()

        # firefox specific preference
        profile.set_preference(
            'browser.privatebrowsing.autostart', True)
        # disable webrtc. This feature is possible only for firefox
        profile.set_preference('media.peerconnection.enabled', False)
        capa['pageLoadStrategy'] = self.load_strategy
        capa['acceptInsecureCerts'] = self.insecure_certs
        profile.set_preference(
                'general.useragent.override', f'{self.user_agent}')

        if self.proxy:
            capa['proxy'] = {
                'proxyType': 'manual',
                'httpProxy': self.proxy,
                'sslProxy': self.proxy
            }

        options.set_headless()
        driver = webdriver.Firefox(firefox_profile=profile,
                                   capabilities=capa,
                                   options=options,
                                   executable_path=path,
                                   firefox_binary=binary,
                                   log_path=self.log_path)
        driver.implicitly_wait(self.timeout)
        driver.set_page_load_timeout(self.timeout)
        driver.set_window_size(*self.screen)

        return driver

    def init_remote_chrome(self, server_url):

        # TODO  Need to test if it necessary
        options = ChromeOptions()
        capa = DesiredCapabilities.CHROME.copy()

        # configure options
        options.set_headless()
        # chrome specific preference
        options.add_argument('--no-sandbox')
        # options.binary_location = binary
        options.add_argument(f'user-agent={self.user_agent}')
        if self.proxy:
            options.add_argument(f'--proxy-server={self.proxy}')

        # configure desire capabilities
        capa['pageLoadStrategy'] = self.load_strategy
        capa['acceptInsecureCerts'] = self.insecure_certs

        driver = RemoteWebDriver(command_executor=server_url,
                                 desired_capabilities=capa,
                                 options=options)

        return driver

# TODO Fix logic of exceptions.

# Chrome crashed often when trying to save screenshotsh with exceptions:
# WebDriverException unknown error: session deleted because of page crash
# from tab crashed. Fixed with add shm host volume.

# Sometimes chrome crashed while initialize it with exception:
# ConnectionResetError: [Errno 104] Connection reset by peer.
# Reinit helps to fixed it.

