import time
from abc import ABC, abstractmethod

from lxml import etree


class AbstractCrawler(ABC):

    def __init__(self, url, webdriver, timeout=15):
        """
        :param url: target url
        :type url: str

        :param webdirver: Selenium webdriver instance
        :type webdriver: selenium.webdriver.WebDriver

        """
        webdriver.get(url)
        # time.sleep(timeout) 
        self.browser = webdriver
        self.timeout = timeout

    @abstractmethod
    def find_by_xpath(self, xpath, attr=None):
        pass

    @abstractmethod
    def find_by_css(self, css, attr=None):
        pass

    def take_binary_screenshot(self):
        self.browser.refresh()
        time.sleep(self.timeout)
        return self.browser.get_screenshot_as_png()

    def find_by_tags(self, tags):
        """
        :param tags: Dictionary with keys are 'tag', 'sibling_tag',
        'sibling_value'.
        :type tags: dict[3](str:str)

        """
        pass

class SeleniumCrawler(AbstractCrawler):
    """ Selenium API based crawler. For extracting used only
    webdrivers methods. Applicable for almos any cases.
    """
    __name__ = 'Selenium Crawler'

    def find_by_xpath(self, xpath, attr=None):
        time.sleep(self.timeout) 
        obj = self.browser.find_element_by_xpath(xpath)
        if attr:
            return obj.get_attribute(attr)
        return obj.text

    def find_by_css(self, css, attr=None):
        time.sleep(self.timeout)
        obj = self.browser.find_elements_by_css_selector(css)
        if attr:
            return obj.get_attribute(attr)
        return obj.text


class LxmlCrawler(AbstractCrawler):
    """ Selenium and lxml based crawler. For extracting used lxml
    methods, such xpath and cssselector. Applicalbe when we used
    JavaScript executer and will get raw html.
    """
    __name__ = 'Lxml Crawler'

    def __init__(self, url, webdriver, js_script):
        super().__init__(url, webdriver)
        page_source = webdriver.execute_script(js_script)
        self.lxml_root = etree.HTML(page_source)

    def find_by_xpath(self, xpath, attr=None):
        xpath_tail = '/text()' if not attr else f'/@{attr}'
        return self.lxml_root.xpath(xpath + xpath_tail)[0]

    def find_by_css(self, css, attr=None):
        elm = self.lxml_root.cssselect(css)[0]
        return elm.attrib.get(attr) if attr else elm.text
